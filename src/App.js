import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import Cart from "./components/Cart/Cart";
import Layout from "./components/Layout/Layout";
// import Notification from "./components/Notification/Notification";
import Products from "./components/Shop/Products";
import { fetchCartData, sendCartData } from "./store/cart-actions";

let isInt = true;

function App() {
  const isShow = useSelector((state) => state.ui.isVisible);
  const cart = useSelector((state) => state.cart);
  const dispatch = useDispatch();

  useEffect(() => {
    if (isInt) {
      dispatch(fetchCartData());
      isInt = false;
      return;
    }
    if (cart.changed) {
      dispatch(sendCartData(cart));
    }
  }, [cart, dispatch]);

  return (
    <>
      {/* <Notification /> */}
      <Layout>
        {isShow && <Cart />}
        <Products />
      </Layout>
    </>
  );
}

export default App;
