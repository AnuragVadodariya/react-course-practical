import ProductItem from "./ProductItem";
import classes from "./Products.module.css";

const Products = (props) => {
  const DUMMY_PRODUCTS = [
    {
      id: "p1",
      price: 6,
      title: "Book 1",
      description: "This is a first product!",
    },
    {
      id: "p2",
      price: 10,
      title: "Book 2",
      description: "This is a second product!",
    },
    {
      id: "p3",
      price: 12,
      title: "Book 3",
      description: "This is good product!",
    },
  ];

  return (
    <section className={classes.products}>
      <h2>Buy your favorite products</h2>
      <ul>
        {DUMMY_PRODUCTS.map(({ id, price, title, description }) => (
          <ProductItem
            title={title}
            price={price}
            description={description}
            id={id}
            key={id}
          />
        ))}
      </ul>
    </section>
  );
};

export default Products;
