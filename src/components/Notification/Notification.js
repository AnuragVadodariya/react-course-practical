import React from "react";
import classes from "./Notification.module.css";

const Notification = ({ title = "Success!", otherMessage='Test...' }) => {
  return (
    <div className={classes.notification__wraper}>
      <h3 className={classes.text}>{title}</h3>
      {otherMessage && <h3 className={classes.text}>{otherMessage}</h3>}
    </div>
  );
};

export default Notification;
