import { cartAction } from "./cart-slice";

export const fetchCartData = () => {
  return async (dispatch) => {
    const fetchData = async () => {
      const response = await fetch(
        "https://udemy-course-learning-default-rtdb.firebaseio.com/cart.json"
      );

      if (!response.ok) return [];

      const data = await response.json();

      return data;
    };

    try {
      const data = await fetchData();

      dispatch(
        cartAction.replaceCart({
          items: data.items,
          totalQuantity: data.totalQuantity,
        })
      );
    } catch (error) {
      console.log("error >>> ", error);
    }
  };
};

export const sendCartData = (cart) => {
  return async (dispatch) => {
    const sendRequest = async () => {
      const response = await fetch(
        "https://udemy-course-learning-default-rtdb.firebaseio.com/cart.json",
        { method: "PUT", body: JSON.stringify(cart) }
      );

      if (!response.ok) return;
    };
    try {
      await sendRequest();
    } catch (error) {
      console.log("error >>> ", error);
    }
  };
};
